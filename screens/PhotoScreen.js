import React, { useEffect, useState } from "react";
import { ScrollView, PermissionsAndroid, Platform, Image } from "react-native";
import CameraRoll from "@react-native-community/cameraroll";

async function hasAndroidPermission() {
	const permission = PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE;

	const hasPermission = await PermissionsAndroid.check(permission);
	if (hasPermission) {
		return true;
	}

	const status = await PermissionsAndroid.request(permission);
	return status === "granted";
}
async function loadPicture() {
	if (Platform.OS === "android" && !(await hasAndroidPermission())) {
		return;
	}

	return CameraRoll.getPhotos({ first: 10 });
}

const PhotoScreen = () => {
	const [photos, setPhotos] = useState([]);
	useEffect(() => {
		loadPicture().then((res) => {
			setPhotos(res.edges);
			console.log("loaded photos", res);
		});
		console.log("loading photos");
	}, []);

	return (
		<ScrollView>
			{photos.map((p, i) => (
				<Image
					key={i}
					source={{ uri: p.node.image.uri }}
					style={{ width: 500, height: 500 }}
				></Image>
			))}
		</ScrollView>
	);
};

export default PhotoScreen;
