import React, { useEffect } from "react";
import PropTypes from "prop-types";
import { View, Text, Button } from "react-native";
import t from "../i18n";

const HomeScreen = ({ navigation, route }) => {
	useEffect(() => {}, [route.params?.result]);

	const routeValue = route.params?.result;

	return (
		<View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
			<Text>{t("home.welcome", { appName: "RNegade" })}</Text>
			<Text>Route value = {routeValue} </Text>
			<Button
				onPress={() =>
					navigation.navigate("Details", {
						itemId: 27,
						otherParam: "I come from Home",
					})
				}
				title="Go to details"
			></Button>
			<Button
				title="Photo"
				onPress={() => {
					navigation.navigate("Photos");
				}}
			></Button>
			<Button
				title="Avatar"
				onPress={() => {
					navigation.navigate("Avatar");
				}}
			></Button>
		</View>
	);
};
HomeScreen.propTypes = {
	navigation: PropTypes.object,
	route: PropTypes.object,
};

export default HomeScreen;
