import "react-native-gesture-handler";
import React, { useState } from "react";
import { StyleSheet, View, Platform, PlatformColor } from "react-native";
import Header from "./components/Header";
import StartGameScreen from "./screens/StartGameScreen";
import GameScreen from "./screens/GameScreen";
import GameOverScreen from "./screens/GameOverScreen";
import HomeScreen from "./screens/HomeScreen";
import PhotoScreen from "./screens/PhotoScreen";
import AvatarScreen from "./screens/AvatarScreen";
import DetailsScreen from "./screens/DetailsScreen";
import ThemeContextProvider from "./contexts/ThemeContext";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import t from "./i18n";

const Stack = createStackNavigator();

const App = () => {
	const [userNumber, setUserNumber] = useState();
	const [guessRounds, setGuessRounds] = useState(0);

	const newGameHandler = () => {
		setGuessRounds(0);
		setUserNumber(null);
	};

	const startGameHandler = (selectedNumber) => {
		setUserNumber(selectedNumber);
		setGuessRounds(0);
	};

	const gameOverHandler = (numOfRounds) => {
		setGuessRounds(numOfRounds);
	};

	let content = <StartGameScreen onStartGame={startGameHandler} />;
	if (userNumber && guessRounds <= 0) {
		content = (
			<GameScreen userChoice={userNumber} onGameOver={gameOverHandler} />
		);
	} else if (guessRounds > 0) {
		content = (
			<GameOverScreen
				rounds={guessRounds}
				userNumber={userNumber}
				onRestart={newGameHandler}
			/>
		);
	}

	return (
		<NavigationContainer>
			<Stack.Navigator
				initialRouteName="Home"
				screenOptions={{
					title: t("navigation.home"),
					headerStyle: {
						...Platform.select({
							ios: {
								backgroundColor: PlatformColor("systemBackground"),
							},
						}),
					},
					...Platform.select({
						ios: {
							headerTintColor: PlatformColor("label"),
						},
					}),
				}}
			>
				<Stack.Screen name="Home" component={HomeScreen} />
				<Stack.Screen
					name="Details"
					options={{ title: t("navigation.details") }}
					component={DetailsScreen}
				/>
				<Stack.Screen
					name="Photos"
					options={{ title: "Camera roll" }}
					component={PhotoScreen}
				/>
				<Stack.Screen
					name="Avatar"
					options={{ title: "Avatar" }}
					component={AvatarScreen}
				/>
			</Stack.Navigator>
			<View style={styles.header}>
				<ThemeContextProvider>
					<Header title="Guess a Number"></Header>
				</ThemeContextProvider>
				{content}
			</View>
		</NavigationContainer>
	);
};

const styles = StyleSheet.create({
	header: {
		flex: 1,
	},
});

export default App;
