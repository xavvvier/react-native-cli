import React, { useState, useEffect } from "react";
import { View, TextInput, Text, Button } from "react-native";
import PropTypes from "prop-types";

const DetailsScreen = ({ route, navigation }) => {
	const [detailValue, setDetailValue] = useState("");
	const { itemId, otherParam } = route.params || {};
	const extraText = itemId ? (
		<Text>
			{itemId}: {otherParam}
		</Text>
	) : null;

	useEffect(() => {
		const randomValue = Math.floor(Math.random() * Math.floor(100));
		setDetailValue(String(randomValue));
	}, [route]);
	return (
		<View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
			<Text>Details Screen</Text>
			{extraText}
			<Button
				onPress={() => navigation.navigate("Home", { result: detailValue })}
				title="Take me out of here"
			></Button>
			<Button
				title="Nested detail"
				onPress={() => navigation.push("Details")}
			></Button>
			<TextInput
				placeholder="A computed value"
				value={detailValue}
				onChangeText={setDetailValue}
			></TextInput>
		</View>
	);
};
DetailsScreen.propTypes = {
	navigation: PropTypes.object,
	route: PropTypes.object,
};

export default DetailsScreen;
