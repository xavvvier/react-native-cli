import React, { useState } from "react";
import { SafeAreaView, ScrollView, View, StyleSheet } from "react-native";
import MapView from "react-native-maps";
import { Button, CheckBox, Divider, Input } from "react-native-elements";

const border = {
	borderWidth: 1,
};

const styles = StyleSheet.create({
	map: {
		...StyleSheet.absoluteFillObject,
	},
});

const LayoutApp = () => {
	const [checked, setChecked] = useState(true);
	return (
		<SafeAreaView
			style={[
				border,
				{
					borderColor: "blue",
					flex: 1,
					alignItems: "flex-start",
					alignContent: "flex-start",
					flexDirection: "row",
					flexWrap: "wrap",
				},
			]}
		>
			<View
				style={[
					border,
					{
						flex: 1,
						height: 300,
						width: 100,
						minHeight: 500,
						minWidth: 400,
						flexWrap: "wrap",
						flexGrow: 1,
						borderColor: "purple",
					},
				]}
			>
				<MapView
					style={styles.map}
					initialRegion={{
						latitude: 37.78825,
						longitude: -122.4324,
						latitudeDelta: 0.0922,
						longitudeDelta: 0.0421,
					}}
				/>
			</View>
			<ScrollView
				style={[
					border,
					{
						flex: 1,
						width: 100,
						minWidth: 30,
						borderColor: "red",
						flexGrow: 1,
						padding: 10,
					},
				]}
			>
				<View>
					<Button title="Hola" type="outline" raised={true} />
				</View>
				<View>
					<CheckBox title="Receive emails" checked={true} center />
					<Divider style={{}} />
					<CheckBox
						center
						title="Click Here"
						checkedIcon="dot-circle-o"
						uncheckedIcon="circle-o"
						checked={checked}
					/>

					<CheckBox
						center
						title="Click Here to Remove This Item"
						iconRight
						iconType="material"
						checkedIcon="clear"
						uncheckedIcon="add"
						checkedColor="red"
						checked={checked}
						onPress={() => setChecked(!checked)}
					/>
					<Input
						placeholder="INPUT WITH ICON"
						leftIcon={{ type: "font-awesome", name: "chevron-left" }}
					/>

					<Input placeholder="Password" secureTextEntry={true} />
				</View>
			</ScrollView>
		</SafeAreaView>
	);
};

export default LayoutApp;
