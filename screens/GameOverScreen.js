import React from "react";
import { Text, View, Button, StyleSheet } from "react-native";
import PropTypes from "prop-types";

const GameOverScreen = (props) => {
	return (
		<View style={styles.screen}>
			<Text>The Game is Over!</Text>
			<Text>Number of rounds: {props.rounds}</Text>
			<Text>Number was: {props.userNumber}</Text>
			<Button title="NEW GAME" onPress={props.onRestart} />
		</View>
	);
};

GameOverScreen.propTypes = {
	rounds: PropTypes.number,
	userNumber: PropTypes.number,
	onRestart: PropTypes.func,
};
const styles = StyleSheet.create({
	screen: {
		flex: 1,
		justifyContent: "center",
		alignItems: "center",
	},
});

export default GameOverScreen;
