import React, { createContext, useState } from "react";
import PropTypes from "prop-types";
import Colors from "../constants/colors";

const dark = {
	foreground: Colors.primary,
	background: Colors.black,
};
const light = {
	foreground: Colors.accent,
	background: Colors.white,
};

export const ThemeContext = createContext();

const ThemeContextProvider = (props) => {
	const [isLightTheme, setIsLightTheme] = useState(true);
	const [theme, setTheme] = useState({});
	const toggleTheme = () => {
		setIsLightTheme(!isLightTheme);
		setTheme(isLightTheme ? dark : light);
	};

	const providerValue = { theme, isLightTheme, toggleTheme };
	return (
		<ThemeContext.Provider value={providerValue}>
			{props.children}
		</ThemeContext.Provider>
	);
};

ThemeContextProvider.propTypes = {
	children: PropTypes.node,
};

export default ThemeContextProvider;
