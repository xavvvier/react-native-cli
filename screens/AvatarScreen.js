import ImagePicker from "react-native-image-picker";
import React, { useState, useEffect } from "react";
import { Image, View, Text, Button } from "react-native";

// More info on all the options is below in the API Reference... just some common use cases shown here
const options = {
	title: "Select Avatar",
	customButtons: [{ name: "fb", title: "Choose Photo from Facebook" }],
	storageOptions: {
		skipBackup: true,
		path: "images",
	},
};

const AvatarScreen = () => {
	const [showDialog, setShowDialog] = useState(false);
	const [imageUri, setImageUri] = useState();
	useEffect(() => {
		if (!showDialog) return;
		/**
		 * The first arg is the options object for customization (it can also be null or omitted for default options),
		 * The second arg is the callback which sends object: response (more info in the API Reference)
		 */
		ImagePicker.showImagePicker(options, (response) => {
			if (response.didCancel) {
				console.log("User cancelled image picker");
			} else if (response.error) {
				console.log("ImagePicker Error: ", response.error);
			} else if (response.customButton) {
				console.log("User tapped custom button: ", response.customButton);
			} else {
				console.log(Object.keys(response));
				console.log(response.uri);
				setImageUri(response.uri);
			}
		});
	}, [showDialog]);
	return (
		<View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
			<Text>Avatar</Text>
			<Button title="Choose" onPress={() => setShowDialog(true)}></Button>
			{imageUri && (
				<Image
					source={{ uri: imageUri }}
					style={{ width: 300, height: 300 }}
				></Image>
			)}
			{imageUri && <Text>{imageUri}</Text>}
		</View>
	);
};

export default AvatarScreen;
