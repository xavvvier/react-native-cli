import React, { useContext } from "react";
import {
	View,
	Text,
	StyleSheet,
	Button,
	Platform,
	PlatformColor,
} from "react-native";
import PropTypes from "prop-types";
import { ThemeContext } from "../contexts/ThemeContext";

const Header = (props) => {
	const { theme, isLightTheme, toggleTheme } = useContext(ThemeContext);
	const headerTitleStyle = {
		...styles.headerTitle,
		backgroundColor: theme.backgroundColor,
		color: theme.foreground,
	};
	return (
		<View style={{ ...styles.header }}>
			<Text style={headerTitleStyle}>{props.title}</Text>
			<Button
				onPress={toggleTheme}
				title={isLightTheme ? "Dark" : "Light"}
			></Button>
		</View>
	);
};

Header.propTypes = {
	title: PropTypes.string,
};
const styles = StyleSheet.create({
	header: {
		width: "100%",
		height: 90,
		paddingTop: 36,
		alignItems: "center",
		justifyContent: "center",
		flexDirection: "row",
		...Platform.select({
			ios: {
				backgroundColor: PlatformColor("systemBackground"),
			},
			android: {
				// backgroundColor: PlatformColor("@android:color/background_light"),
				backgroundColor: PlatformColor("?colorPrimaryDark"),
			},
			default: { backgroundColor: "black" },
		}),
	},
	headerTitle: {
		fontSize: 18,
	},
});

export default Header;
