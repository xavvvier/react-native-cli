import i18n from "i18n-js";
import * as RNLocalize from "react-native-localize";

const [{ languageCode }] = RNLocalize.getLocales();
const translations = { en: {}, es: {} };
switch (languageCode) {
	case "es":
		translations.es = require("./locales/es.json");
		break;
	default:
		translations.en = require("./locales/en.json");
}

i18n.defaultLocale = "en";
i18n.locale = languageCode;
i18n.fallbacks = true;
i18n.translations = translations;

const t = i18n.t;
export default t;
